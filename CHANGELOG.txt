CHANGELOG
=========

Version 7.x-1.2
- Added "web-" prefix to allow free and anonymous use.

Version 7.x-1.1
- Added README.txt and CHANGELOG.txt files.

Version 7.x-1.0
- Initial release

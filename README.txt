Mail Tester
===========

This is a simple module that uses the simplicity of www.mail-tester.com to help
you improve the quality of your email system.

If you want to know more about Mail Tester, please check their FAQ at
http://www.mail-tester.com/faq

Once the module is enabled, you can go to Administration -> Reports ->
Mail Tester and send a test message, if it is successfully sent, you will be
redirected to Mail Tester website to check what does the internet think about
your email server configuration.
